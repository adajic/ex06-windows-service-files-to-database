﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceProcess;

namespace Control_windows_service
{
    public partial class Form1 : Form
    {
        public String serviceName = "Files to database service";

        public Form1()
        {
            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            ServiceController sc = new ServiceController(serviceName);
            if (sc.Status == ServiceControllerStatus.Stopped)
                sc.Start();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            ServiceController sc = new ServiceController(serviceName);
            if (sc.Status != ServiceControllerStatus.Stopped)
                sc.Stop();
        }
    }
}
