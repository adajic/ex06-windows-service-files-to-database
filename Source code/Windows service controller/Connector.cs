﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Windows_service_controller
{
    class Connector
    {
        SqlConnection conn;
        public Connector(String connectionString)
        {
            conn = new SqlConnection();
            conn.ConnectionString = connectionString;
        }

        public void changeSqlConnectionStringBuilder(String connectionString)
        {
            conn.ConnectionString = connectionString;
        }

        public void testConnection()
        {
            // Throws exception in case of any problem, and if there is no required table 'files'
            try
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("SELECT count(*) as Exist from INFORMATION_SCHEMA.TABLES where table_name = 'files'", conn))
                {
                    int numberOfRecords = Convert.ToInt32(command.ExecuteScalar());
                    if (numberOfRecords==0)
                        throw new Exception("Required table 'files' doesn't exist");                    
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw (ex);
            }
        }
    }
}
