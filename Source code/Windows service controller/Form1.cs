﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Data.ConnectionUI;
using System.ServiceProcess;

namespace Windows_service_controller
{
    public partial class Form1 : Form
    {
        bool folderPathLoaded;
        bool connectionStringLoaded;
        String folderPath;
        String connectionString;
        String serviceName;

        String filePath;
        public Form1()
        {
            InitializeComponent();
            serviceName = "Files to database service"; // This name is used to control the service
            filePath = Environment.GetEnvironmentVariable("TEMP", EnvironmentVariableTarget.Machine) + "\\Files_to_database_windows_service.txt";
            folderPathLoaded = false;
            connectionStringLoaded = false;
            loadDataFromFile();
            if (!folderPathLoaded || !connectionStringLoaded)
            {
                while (!folderPathLoaded)
                {
                    showNotification("Please select the folder! Files from that folder will be stored into the database every 15 seconds.");
                    loadFolderPath();
                }
                while (!connectionStringLoaded)
                {
                    showNotification("Please enter correct database connection details!");
                    loadConnectionString();
                }
                writeDataToFile();
            }
            showNotification("\"Files to database service\" controller is running in the system tray. Right click the tray icon for options.");
        }

        private void writeDataToFile()
        {
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.WriteLine(folderPath);
                writer.WriteLine(connectionString);
                writer.Close();
            }
        }

        private void showNotification(String message)
        {
            notifyIcon2.BalloonTipTitle = "Notification";
            notifyIcon2.BalloonTipText = message;
            notifyIcon2.ShowBalloonTip(1000);
        }

        private void showMsgBoxInfo(String message)
        {
            MessageBox.Show(message, "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void loadConnectionString()
        {
            DataConnectionDialog dcd = new DataConnectionDialog();
            DataSource.AddStandardDataSources(dcd);
            dcd.SelectedDataSource = DataSource.SqlDataSource;
            dcd.SelectedDataProvider = DataProvider.SqlDataProvider;
            if (DataConnectionDialog.Show(dcd) == DialogResult.OK)
            {
                if (testConnection(dcd.ConnectionString))
                {
                    connectionStringLoaded = true;
                    connectionString = dcd.ConnectionString;
                }
            }
            else
            {
                showNotification("Database connection details are required. Program is exiting...");
                Environment.Exit(1);
            }
        }

        private void loadFolderPath()
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK)
                {
                    if (Directory.Exists(fbd.SelectedPath)) { 
                        folderPathLoaded = true;
                        folderPath = fbd.SelectedPath;
                    }
                } else
                {
                    showNotification("Choosing folder is required. Program is exiting...");
                    Environment.Exit(1);
                }
            }
        }

        private void loadDataFromFile()
        {
            if (!File.Exists(filePath)) return;
            // File exists
            using (StreamReader sr = File.OpenText(filePath))
            {
                String s = "";
                int lineNum = 0;
                while ((s = sr.ReadLine()) != null)
                {
                    lineNum++;
                    if (lineNum == 1)
                    {
                        if (Directory.Exists(s))
                        {
                            folderPathLoaded = true;
                            folderPath = s;
                        }
                    }
                    else if (lineNum == 2)
                    {
                        if (testConnection(s))
                        {
                            connectionStringLoaded = true;
                            connectionString = s;
                        }
                    }
                    else break;
                }
            }
        }

        private bool testConnection(String connStr)
        {
            try
            {
                Connector connector = new Connector(connStr);
                connector.testConnection();
                return true;
            }
            catch (Exception ex)
            {
                showNotification(ex.Message);
                return false;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMsgBoxInfo("'Files to database service' is a service which stores files from chosen directory into a database. This is controller of the service." + Environment.NewLine + Environment.NewLine + "Version 1.0.0");
        }

        private void startTheServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (folderPathLoaded && connectionStringLoaded)
                {
                    ServiceController sc = new ServiceController(serviceName);
                    if (sc.Status != ServiceControllerStatus.Running)
                    {
                        sc.Start();
                        showNotification("The service is running");
                    }
                    else
                    {
                        showNotification("The service is already running");
                    }
                }
                else
                {
                    showNotification("Please first load folder path and database connection details");
                }
            } catch (Exception ex)
            {
                showNotification(ex.Message);
            }
        }

        private void stopTheServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceController sc = new ServiceController(serviceName);
                if (sc.Status != ServiceControllerStatus.Stopped)
                {
                    sc.Stop();
                    showNotification("The service is stopped");
                }
                else
                {
                    showNotification("The service is already stopped");
                }
            } catch (Exception ex)
            {
                showNotification(ex.Message);
            }
        }
    }
}
