﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Windows_service
{
    public partial class Service1 : ServiceBase
    {
        private System.Timers.Timer m_mainTimer;
        private String logPath;
        private String filePath;
        private bool debug;

        public Service1()
        {
            InitializeComponent();
            logPath = Environment.GetEnvironmentVariable("TEMP", EnvironmentVariableTarget.Machine) + "\\Files_to_database_windows_service_log.txt";
            filePath = Environment.GetEnvironmentVariable("TEMP", EnvironmentVariableTarget.Machine) + "\\Files_to_database_windows_service.txt";
            debug = false;
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        private void appendLogMessage(String msg)
        {
            using (StreamWriter stream = File.AppendText(logPath))
            {
                stream.WriteLine(msg);
            }
        }
        public static String getTimeDate()
        {
            String DateTime = System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            return DateTime;
        }

        protected override void OnStart(string[] args)
        {
            if (debug) appendLogMessage("Started service: " + getTimeDate());
            try
            {
                //
                // Create and start a timer.
                //
                m_mainTimer = new System.Timers.Timer();
                m_mainTimer.Interval = 20000;   // every 20 secs
                m_mainTimer.Elapsed += m_mainTimer_Elapsed;
                m_mainTimer.AutoReset = false;  // makes it fire only once
                m_mainTimer.Start(); // Start
                doTask();
            }
            catch (Exception ex)
            {
                appendLogMessage("Exception in OnStart: " + getTimeDate());
                appendLogMessage("Message: " + ex.Message);
            }
        }

        protected override void OnStop()
        {
            if (debug) appendLogMessage("Stopped service: " + getTimeDate());
            try
            {
                // Service stopped. Also stop the timer.
                m_mainTimer.Stop();
                m_mainTimer.Dispose();
                m_mainTimer = null;
            }
            catch (Exception ex)
            {
                appendLogMessage("Exception in OnStop: " + getTimeDate());
                appendLogMessage("Message: " + ex.Message);
            }
        }

        private void doTask()
        {
            if (debug) appendLogMessage("Started task: " + getTimeDate());
            if (!File.Exists(filePath))
            {
                appendLogMessage("Problem: File with folder path and connection details doesn't exist on: " + filePath);
                appendLogMessage("Please run the service through the controller!");
                OnStop();
            }
            // File exists
            bool folderPathLoaded = false;
            String folderPath = "";
            bool connectionStringLoaded = false;
            String connectionString = "";
            using (StreamReader sr = File.OpenText(filePath))
            {
                String s = "";
                int lineNum = 0;
                while ((s = sr.ReadLine()) != null)
                {
                    lineNum++;
                    if (lineNum == 1)
                    {
                        if (Directory.Exists(s))
                        {
                            folderPathLoaded = true;
                            folderPath = s;
                            if (debug) appendLogMessage("Check 1 OK: Folder exists");
                        } else
                        {
                            appendLogMessage("Problem: First line doesn't contain a valid path in file: '" + filePath + "'.");
                            appendLogMessage("Please delete that file and run the controller app!");
                            OnStop();
                        }
                    }
                    else if (lineNum == 2)
                    {
                        if (testConnection(s))
                        {
                            connectionStringLoaded = true;
                            connectionString = s;
                            if (debug) appendLogMessage("Check 2 OK: Database connection is functional");
                        } else
                        {
                            appendLogMessage("Problem: The second line doesn't contain a valid database connection string.");
                            appendLogMessage("Please delete that file and run the controller app!");
                            OnStop();
                        }
                    }
                    else break;
                }
            }
            try
            {
                if (folderPathLoaded && connectionStringLoaded)
                {
                    FileInfo[] files = null;
                    DirectoryInfo folder = new DirectoryInfo(folderPath);
                    files = folder.GetFiles("*");
                    String fullName;
                    if (files.Count() > 0)
                    {
                        Connector connector = new Connector(connectionString);
                        if (debug) appendLogMessage("Found files: " + files.Count().ToString());
                        for (int i = 0; i < files.Count(); i++)
                        {
                            fullName = files[i].FullName;
                            if (debug) appendLogMessage((i + 1).ToString() + ". " + fullName);
                            try
                            {
                                connector.insertFile(fullName);
                                File.Delete(fullName);
                                if (debug) appendLogMessage("File processed successfully.");
                            }
                            catch(Exception ex)
                            {
                                appendLogMessage("Exception processing the file: " + fullName);
                                appendLogMessage("Message: " + ex.Message);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                appendLogMessage("Exception in doTask: " + getTimeDate());
                appendLogMessage("Message: " + ex.Message);
            }
        }

        private bool testConnection(String connStr)
        {
            try
            {
                Connector connector = new Connector(connStr);
                connector.testConnection();
                return true;
            }
            catch (Exception ex)
            {
                appendLogMessage("Exception in testConnection: " + ex.Message);
                return false;
            }
        }

        void m_mainTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                // do some work
                doTask();
                if (m_mainTimer!=null)
                {
                    m_mainTimer.Start();
                }
            }
            catch (Exception ex)
            {
                appendLogMessage("Exception in m_mainTimer_Elapsed: " + getTimeDate());
                appendLogMessage("Message: "+ex.Message);
            }
        }
    }
}
